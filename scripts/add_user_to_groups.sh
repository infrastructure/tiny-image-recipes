#!/bin/sh

set -e

DEFGROUPS="admin,adm,dialout,cdrom,plugdev,audio,dip,video,staff,shared"

for group in $(echo $DEFGROUPS | tr ',' ' '); do
    if grep  -i "^$group" /etc/group ; then
       echo "Group $group exists in /etc/group"
    else
       echo "Group $group does not exist in /etc/group, creating"
       groupadd $group
    fi
done

echo "I: add user to ($DEFGROUPS) groups"
usermod -a -G ${DEFGROUPS} user
