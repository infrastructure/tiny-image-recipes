#!/bin/sh

# /dev must be cleaned outside of chroot since
# systemd-nspawn mounts own '/dev' instance

set -e

if [ -z  "${ROOTDIR}" ] ; then
  echo "ROOTDIR not given"
  exit 1
fi

if [ $(realpath ${ROOTDIR}/dev) = "/dev" ]
then
    echo "Refusing to remove system '/dev'"
    exit 1
fi

rm -rf "${ROOTDIR}"/dev/*
