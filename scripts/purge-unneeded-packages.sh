#!/bin/sh
set -ve

# Crush into a minimal production image to be deployed via some type of image
# updating system. The Debian system is not longer functional at this point.

UNNEEDED_PACKAGES="
adduser
apertis-archive-keyring
apt
apt-transport-https
ca-certificates
debconf
e2fslibs
e2fsprogs
gcc-5-base
gcc-6-base
gnupg
gpgv
gzip
insserv
iptables
libapparmor-perl
libapt-pkg5.0
libasn1-8-heimdal
libaudit-common
libbsd0
libbz2-1.0
libcomerr2
libcryptsetup4
libcurl3
libdb5.3
libdebconfclient0
libedit2
libfuse2
libgmp3c2
libgnutls30
libgssapi3-heimdal
libgssapi-krb5-2
libhcrypto4-heimdal
libheimbase1-heimdal
libheimntlm0-heimdal
libhogweed4
libhx509-5-heimdal
libk5crypto3
libkeyutils1
libkrb5-26-heimdal
libkrb5-3
libkrb5support0
libldap-2.4-2
liblocale-gettext-perl
libncurses5
libncursesw5
libnettle6
libnfnetlink0
libp11-kit0
libpam-modules
libpam-modules-bin
libprocps4
libreadline5
libroken18-heimdal
librtmp1
libsasl2-2
libsasl2-modules-db
libsemanage1
libsemanage-common
libsepol1
libsmartcols1
libsqlite3-0
libss2
libssl1.0.0
libtasn1-6
libtext-charwidth-perl
libtext-iconv-perl
libtext-wrapi18n-perl
libtinfo5
libusb-0.1-4
libustr-1.0-1
libwind0-heimdal
libwrap0
libxtables11
login
lsb-base
lzma
mawk
mktemp
mount
multiarch-support
ncurses-bin
ncurses-base
net-tools
openssh-client
openssh-server
openssh-sftp-server
openssl
passwd
procps
sed
sensible-utils
sysv-rc
tzdata
util-linux
vim-common
vim-tiny
vim-tiny-dbgsym
xz-utils
"

# leave these at the end to avoid errors in the pre/post removal scripts of
# the packages above
UNNEEDED_PACKAGES="
$UNNEEDED_PACKAGES
hostname
perl-base
dpkg
"

# Removing unused packages
for PACKAGE in ${UNNEEDED_PACKAGES}
do
	echo "Forcing removal of ${PACKAGE}"
	if ! dpkg --purge --force-remove-essential --force-depends "${PACKAGE}"
	then
		echo "WARNING: Failed to purge ${PACKAGE}"
	fi
done
