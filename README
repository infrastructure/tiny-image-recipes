General Information
===================

This repository contains Tiny specific recipes for Apertis container creation.

To use these recipes, the debos tool needs to be installed: https://github.com/go-debos/debos

By default, this command will create a skeleton container for Intel 64bit systems:
```
debos tiny-connectivity-container.yaml
```

The target architecture can be changed with command line parameter '-t architecture:armhf'.
Architectures supported:
  * arm64
  * amd64
  * armhf

Parameters for image versioning:
  * suite: (-t suite:v2023)
  * timestamp: (-t timestamp:`date +"%s"`)


Building in docker
==================

To ease testing on a wide variety of host systems (and building in Jenkins) a
docker file is provided which sets up the build environment. To build using
that:

* Pull and run the docker image:

You may pull and run the v2023 image directly with:
```
RELEASE=v2023 # the Apertis release to be built
docker run --device /dev/kvm  -w /recipes \
  -u $(id -u) \
  --group-add=$(getent group kvm | cut -d : -f 3) \
  -i -v $(pwd):/recipes \
  -t registry.gitlab.apertis.org/infrastructure/apertis-docker-images/$RELEASE-image-builder \
  debos -t suite:$RELEASE tiny-connectivity-container.yaml
```

Note: The examples are for the v2023 release. You may change the
      version if working on a different branch.

The image build will be run in the build docker image using your uid with the
additional kvm group (for debian based systems) to ensure debos/fakemachine can
use hardware virtualisation. For fedora/Redhat based systems the group-add is
likely not needed.
