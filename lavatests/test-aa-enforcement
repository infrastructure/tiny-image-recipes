#!/bin/sh

# Copyright © 2019 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# NB: protection of dbus-daemon have no real value but this test is using it to check
# if nested profile could be applied outside and inside of container 

set -ue
. $(dirname $0)/functions

aa_profile_name="/usr/bin/dbus-daemon"

LXC_NAME=${LXC_NAME:=tinytest}
[ "$(id -u)" = 0 ] || LXC_NAME=${LXC_NAME}-$USER

aa_namespace=${aa_namespace:=lxc-apertis-tiny}
imageshost=${imageshost:=images.apertis.org}

get_opts "$@"

# Cleanup container if needed
container_reset

# aa-status is not the part of images
check_aa_applied(){
    local ppid="$(lxc-info -n ${LXC_NAME} -p -H)"
    local aa_label_expected="lxc-container-apertis-${aa_namespace}//&:${aa_namespace}:${aa_profile_name}"

    # Find desired command from container's processes (use only the first available
    read aa_label mode pid command <<E_O_F
$(ps --no-headers -o label,pid,cmd --ppid ${ppid} | grep "$aa_profile_name" | head -n 1)
E_O_F
 
    [ "${aa_label_expected}" = "${aa_label}" ] && return 0
    echo "Not expected AppArmor label: ${aa_label} ${mode} ${command}"

    return 1
}

container_start_bg && container_os_wait

#########################################################
# Check there is no nested profile prior the test
test_name=aa_profile_embedded

if check_aa_applied; then
    pass $test_name
else
    fail $test_name
fi

#########################################################
# Check if applied profile could be cleaned from container
test_name=aa_profile_embedded_cleanup
container_exec apparmor_parser -R "/etc/apparmor.d/usr.bin.dbus-daemon" || fail $test_name
if check_aa_applied >/dev/null; then
    fail $test_name
else
    pass $test_name
fi

exit 0
